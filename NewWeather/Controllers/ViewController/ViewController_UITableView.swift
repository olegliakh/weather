
import Kingfisher
import CoreLocation
import UIKit

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRwPerSection[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DayTableViewCell", for: indexPath) as? DayTableViewCell else { return UITableViewCell() }
            
            cell.bind()
            if let oneWeather = self.viewModel.weather.value?.daily?[indexPath.row + 1] {
                cell.viewModel.configure(withInfo: oneWeather)
            }
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PropertyTableViewCell", for: indexPath) as? PropertyTableViewCell else { return UITableViewCell() }
            cell.bind()
            switch indexPath.row {
            case 0 :
                let result = String(viewModel.weather.value?.current?.humidity ?? 0) + " %"
                cell.viewModel.configure(withResult: result, forName: "Humidity".localized)
            case 1 :
                let result = String(lround( viewModel.weather.value?.current?.uvi ?? 0))
                cell.viewModel.configure(withResult: result, forName: "UV index".localized)
            case 2 :
                let result = String( lround(viewModel.weather.value?.current?.windSpeed ?? 0)) + "m/s".localized
                cell.viewModel.configure(withResult: result, forName: "Wind speed".localized)
            case 3 :
                let time = Double((viewModel.weather.value?.current?.sunrise ?? 0) + (viewModel.weather.value?.timezoneOffset ?? 0) - viewModel.myTimeOffset) .getTimeFromUTC()
                cell.viewModel.configure(withResult: time, forName: "Sunrise".localized)
            case 4 :
                let time = Double((viewModel.weather.value?.current?.sunset ?? 0) + (viewModel.weather.value?.timezoneOffset ?? 0) - viewModel.myTimeOffset).getTimeFromUTC()
                cell.viewModel.configure(withResult: time, forName: "Sunset".localized)

            default :
                cell.configure(withResult: "", forName: "")
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Week forecast".localized
        }
        else {
            return " "
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    
}
