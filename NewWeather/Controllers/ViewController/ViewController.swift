import Kingfisher
import CoreLocation
import UIKit

class ViewController: UIViewController, CLLocationManagerDelegate  {
    
    var viewModel = NewModel()
//    var blurView = UIVisualEffectView()
//    @IBOutlet weak var leadingBurgerConstraint: NSLayoutConstraint!
//    @IBOutlet weak var burgerView: UIView!
    //    var weather : WeatherData?
    let iconURLString = "https://openweathermap.org/img/wn/"
    let trailIconURL = "@4x.png"
    
    @IBOutlet weak var currentLocButton: UIButton!
//    @IBOutlet weak var savedCitiesTableView: UITableView!
    @IBOutlet weak var perDayTableView: UITableView!
    @IBOutlet weak var perHourCollectionView: UICollectionView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var backgroundImageView: UIImageView!
//    @IBOutlet weak var burgerButton: UIButton!
    @IBOutlet weak var todayTemperature: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var currentWeatherImageView: UIImageView!
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    
    
    var locationForRequest : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 55.751244, longitude: 37.618423)
    let locationManager = CLLocationManager()
    var isFavourite = false
    var isCurrentLocation = true
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        showLoadingIndicator()
        launchLocation()
        bind()
        loadWeatherForCurrentLocation()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadBgView()
        updateCurrentButton()
        showLoadingIndicator()
//        perDayTableView.autoresizingMask = [.flexibleHeight]

    }
    
    @IBAction func currentLocButtonPressed(_ sender: UIButton) {
        if !isCurrentLocation {
            if let location = locationManager.location?.coordinate {
                viewModel.setLocation(location: location)
                isCurrentLocation.toggle()
                updateCurrentButton()
            }
        }
    }
    
    func updateCurrentButton() {
        currentLocButton.setTitle(isCurrentLocation ? "Current location".localized : "Load current location".localized, for: .normal)
        currentLocButton.setImage(isCurrentLocation ? UIImage(systemName: "location.fill") : UIImage(systemName: "location"), for: .normal)
    }
    
    func loadWeatherForCurrentLocation() {
        viewModel.loadCityName()
        viewModel.loadWeatherData()
        isCurrentLocation = true
    }
    
    func launchLocation() {
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func loadBgView() {
        let newURLString = "https://i.pinimg.com/originals/45/ce/29/45ce2986d79fc7cd05014bd522a88834.jpg"
        let URL = URL(string: newURLString)
        self.backgroundImageView.kf.setImage(with: URL)
        backgroundImageView.contentMode = .scaleToFill
    }
    
    private func bind () {
        self.viewModel.location.bind { _ in
            self.viewModel.loadCityName()
            self.viewModel.loadWeatherData()
            
        }
        self.viewModel.cityName.bind { cityName in
            DispatchQueue.main.async {
                self.cityLabel.text = cityName
            }
        }
        self.viewModel.weather.bind { weatherData in
            DispatchQueue.main.async {
//                self.hideLoadingIndicator()
                self.currentTemperatureLabel.text = String(Int(weatherData?.hourly?.first?.temp ?? 0)) + "°"
                self.dateLabel.text = Double(weatherData?.current?.dt ?? 0).getDateStringFromUTC()
                let dayTemp = String(Int(weatherData?.daily?.first?.temp?.day ?? 0))
                let nightTemp = String(Int(weatherData?.daily?.first?.temp?.night ?? 0))
                let feelsTemp = String(Int(weatherData?.hourly?.first?.feelsLike ?? 0))
                self.todayTemperature.text =  dayTemp + "°/ " + nightTemp +  "° " + "Feels like".localized + feelsTemp + "°"
                let iconName = (weatherData?.current?.weather?.first?.icon ?? "")
                let newURLString = self.iconURLString + iconName + self.trailIconURL
                let URL = URL(string: newURLString)
                self.currentWeatherImageView.kf.setImage(with: URL)
                                
                self.perHourCollectionView.reloadData()
                self.perDayTableView.reloadData()
                self.hideLoadingIndicator()

            }
//            self.hideLoadingIndicator()
        }
    }
    
    @IBAction func addCityButtonPressed(_ sender: UIButton) {
//        hideBurgerMenu()
        guard let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController else { return }
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
//    @IBAction func burgerButtonPressed(_ sender: UIButton) {
//        blurView.frame = self.view.bounds
//        blurView.backgroundColor = .clear
//        blurView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeBurgerView(_:))))
//        self.view.addSubview(blurView)
//        UIView.animate(withDuration: 0.3) {
//            self.leadingBurgerConstraint.constant = 0
//        }
//        self.view.bringSubviewToFront(burgerView)
//    }
//    @IBAction func closeBurgerView( _ sender: UITapGestureRecognizer) {
//        hideBurgerMenu()
//    }
//    func hideBurgerMenu() {
//        UIView.animate(withDuration: 0.3) {
//            self.leadingBurgerConstraint.constant = 0 - self.burgerView.frame.width
//        } completion: { _ in
//            self.blurView.removeFromSuperview()
//        }
//    }
   
    
    func showLoadingIndicator() {
        for oneView in [todayTemperature, dateLabel, cityLabel, currentWeatherImageView, currentTemperatureLabel, perHourCollectionView, perDayTableView] {
            oneView?.isHidden = true
        }
        spinner.isHidden = false
    }
    
    func hideLoadingIndicator() {
        for oneView in [todayTemperature, dateLabel, cityLabel, currentWeatherImageView, currentTemperatureLabel, perHourCollectionView, perDayTableView] {
            oneView?.isHidden = false
        }
        spinner.isHidden = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locationManager.location?.coordinate {
            self.viewModel.setLocation(location: location)
        }
        else {
            self.viewModel.setLocation(location: locationForRequest)
        }
        self.locationManager.stopUpdatingLocation()
    }
}

extension ViewController : SearchViewControllerDelegate {
    
    func updateInfoForCity(withName name : String) {
        self.viewModel.setCityName(name)
        self.isCurrentLocation = false
    }
}

