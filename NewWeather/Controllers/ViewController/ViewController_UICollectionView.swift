
import Kingfisher
import CoreLocation
import UIKit

extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.numberOfHourItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourCell", for: indexPath) as? HourCollectionViewCell else { return UICollectionViewCell() }
        
        cell.bind()
        if let oneHourWeather = self.viewModel.weather.value?.hourly![indexPath.item + 1] {
            cell.viewModel.configure(withInfo: oneHourWeather, cityTimeOffset: (self.viewModel.weather.value?.timezoneOffset ?? 0) , myTimeOffset: self.viewModel.myTimeOffset)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 65, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}
