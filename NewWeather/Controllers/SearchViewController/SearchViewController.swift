
import CoreLocation
import UIKit

protocol SearchViewControllerDelegate {
    func updateInfoForCity(withName: String)
}

class SearchViewController: UIViewController {
    
    var viewModel = SearchViewModel()
    
    var delegate : SearchViewControllerDelegate?
    @IBOutlet weak var citiesTableView: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    var searchBarIsEmpty : Bool {
        guard let text = searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    var isFiltering : Bool {
        return searchController.isActive && !searchBarIsEmpty
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadCitiesFromApi()
//        viewModel.loadCitiesInformation()
//        searchBar.isHidden = true
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search".localized
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    func bind() {
        self.viewModel.loadedCities.bind { cities in
            DispatchQueue.main.async {
                self.citiesTableView.reloadData()
            }
        }
    }
}

extension SearchViewController : UISearchResultsUpdating {
        
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text {
            filterContentForSearchText(text)
        }
    }
    
    func filterContentForSearchText(_ searchText: String) {
        viewModel.filteredCity.value = viewModel.loadedCities.value.filter({ city in
            guard let cityUnwr = city else {return false}
            return cityUnwr.lowercased().contains(searchText.lowercased())
            
        })
        citiesTableView.reloadData()
    }
}

