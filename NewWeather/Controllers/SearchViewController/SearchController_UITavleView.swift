import UIKit
import CoreLocation
import Foundation

extension SearchViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return viewModel.filteredCity.value.count ?? 0
        }
        return 0

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CityTableViewCell", for: indexPath) as? CityTableViewCell else { return UITableViewCell() }
        cell.bind()
        if isFiltering {
            if let name = self.viewModel.filteredCity.value[indexPath.row] {
                cell.viewModel.configure(name: name)
            }
        } else {
            if let name = self.viewModel.loadedCities.value[indexPath.row] {
                cell.viewModel.configure(name: name)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("trying select")
        guard let cell = tableView.cellForRow(at: indexPath) as? CityTableViewCell else { return }
           if let name = cell.viewModel.name.value {
            print("I'm in did select")
               delegate?.updateInfoForCity(withName: name)
            self.navigationController?.popViewController(animated: true)
        }
    }
}
