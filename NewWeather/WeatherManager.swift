import Foundation
import CoreLocation

class WeatherManager {
    
    static let shared = WeatherManager()
    let myAPIKey = "8a66b4726a5c20e25d3fdd7a014de51d"
    
    
    func getCoordinates(byName name: String, completion: @escaping(Double, Double)->()) {
        let urlString = "https://api.openweathermap.org/geo/1.0/direct?q=\(name)&limit=5&appid=\(myAPIKey)"
        
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url, timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            //                print(String(data:data, encoding: .utf8)!)
            do {
                let object = try JSONDecoder().decode(Welcome.self, from: data)
                if let city = object.first,
                   let lat = city.lat, let lon = city.lon {
                    print(lat, lon)
                    completion(lat, lon)
                }
                
            } catch {
                print("encoding error ((((((")
            }
        }
        task.resume()
    }
    
    func getCityName(byCoordinates coordinate: CLLocationCoordinate2D, completion: @escaping (String)->()) {
        
        let urlString = "https://api.openweathermap.org/geo/1.0/reverse?lat=\(String(coordinate.latitude))&lon=\(String(coordinate.longitude))&units=metric&appid=\(myAPIKey)"
        
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url, timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            //                print(String(data:data, encoding: .utf8)!)
            do {
                let object = try JSONDecoder().decode(Welcome.self, from: data)
                
                //                    print(object.first?.name ?? "...city name")
                let langStr = Locale.current.languageCode
                print(langStr!)
                if let dictionary = object.first?.localNames,
                   let name = dictionary[langStr!] {
                    completion(name)
                }
                else {
                    completion(object.first?.name ?? "...city name")
                }
                //                    completion(object.first?.localNames?[langStr!] ?? "...city name")
                
            } catch {
                print("encoding error ((((((")
            }
        }
        task.resume()
        
    }
    
    
    //    func loadAllCities() {
    //        let apiKey = "daKdDo+X5c2cFhPaz8UNmQ==tlGtC1X4BDJ6yVuD"
    //        guard let url = URL(string: "https://api.api-ninjas.com/v1/city?name=San Francisco") else { return }
    //        var request = URLRequest(url: url)
    //        request.setValue(apiKey, forHTTPHeaderField: "X-Api-Key")
    //        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
    //            guard let data = data else { return }
    //            print("9999999999999999999999999999")
    //            print(String(data: data, encoding: .utf8)!)
    //        }
    //        task.resume()
    //    }
    func loadCities(completion: @escaping ([String])->()) {
        let urlString = "https://countriesnow.space/api/v0.1/countries"
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url, timeoutInterval: .infinity)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            print("Got data!")
            //            print(String(data:data, encoding: .utf8)!)
            do {
                let object = try JSONDecoder().decode(SearchCitiesData.self, from: data)
                //                print("W Manager in object")
                //                print(object)
                //                guard let array = object.data?.flatMap({ country in
                //                    country.cities
                //                })
                //                else { return }
                var array : [String] = []
                if let countries = object.data {
                    for oneCountry in countries {
                        if let cities = oneCountry.cities {
                            array.append(contentsOf: cities)
                        }
                    }
                }
                array.sort()
                //                print("ready for completion")
                completion(array)
                //               completion(object)
                
            } catch {
                print("encoding error ((((((")
            }
        }
        task.resume()
    }
    
    func fetchWeather(oneCity: CLLocationCoordinate2D, completion: @escaping (WeatherData)->()) {
        let urlString = "https://api.openweathermap.org/data/2.5/onecall?lat=\(String(oneCity.latitude))&lon=\(String(oneCity.longitude))&units=metric&appid=\(myAPIKey)"
        
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url, timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            //                print("3333333333333333333")
            //                print(String(data:data, encoding: .utf8)!)
            do {
                let object = try JSONDecoder().decode(WeatherData.self, from: data)
                print(object)
                completion(object)
                
            } catch {
                print("encoding error ((((((")
            }
        }
        task.resume()
        
    }
    
    func getHoutFromDateString(dateString: String) -> Int {
        var formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let date = formatter.date(from: dateString) {
            formatter.dateFormat = "HH"
            return Int(formatter.string(from: date))!
        }
        return 0
    }
}
