

import Foundation

extension Double {
    func getDateStringFromUTC() -> String {
        let date = Date(timeIntervalSince1970: self)

        let dateFormatter = DateFormatter()
//        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateStyle = .medium

        return dateFormatter.string(from: date)
    }
    
    func getHourFromUTC() -> String {
        let date = Date(timeIntervalSince1970: self)
        
        let dateFormatter = DateFormatter()
//        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "HH"

        return dateFormatter.string(from: date)
    }
    
    func getTimeFromUTC() -> String {
        let date = Date(timeIntervalSince1970: self)
        
        let dateFormatter = DateFormatter()
//        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "HH:mm"

        return dateFormatter.string(from: date)
    }
    
    
    func getDayOfWeekFromUTC() -> String {
        let date = Date(timeIntervalSince1970: self)
        
        let dateFormatter = DateFormatter()
//        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "EEEE"

        return dateFormatter.string(from: date)
    }
}


extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
