import UIKit

class PropertyTableViewCell: UITableViewCell {
    
    var viewModel = PropertyCellViewModel()
    
    @IBOutlet weak var propertyLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    
    func configure(withResult result: String, forName name: String ) {
        propertyLabel.text = name
        resultLabel.text = result
    }
    func bind() {
        viewModel.property.bind { property in
            DispatchQueue.main.async {
                self.propertyLabel.text = property
            }
        }
        viewModel.result.bind { result in
            DispatchQueue.main.async {
                self.resultLabel.text = result
            }
        }
    }
    
}
