
import UIKit

class SavedCityTableViewCell: UITableViewCell {
    
    var viewModel = SavedCityModel()

    @IBOutlet weak var cityNameLabel: UILabel!    
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    
    func bind() {
        viewModel.name.bind { name in
            self.cityNameLabel.text = name
        }
    }
    
}
