import UIKit
import Kingfisher

class HourCollectionViewCell: UICollectionViewCell {
    
    var cityTimeOffset : Int?
    
    var viewModel = HourCellViewModel()
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    
    func prepareView() {
        backView.layer.cornerRadius = 10
        backView.layer.borderWidth = 1
        backView.layer.borderColor = UIColor.systemGray4.cgColor
//        backView.clipsToBounds = true
//        backView.addShadow()
    }
    func bind() {
        prepareView()
        viewModel.temp.bind { _ in
            DispatchQueue.main.async {
                self.tempLabel.text = (self.viewModel.temp.value ?? "")
            }
        }
        viewModel.hour.bind { _ in
            DispatchQueue.main.async {
                self.hourLabel.text = self.viewModel.hour.value
            }
        }

        viewModel.image.bind { image in
            DispatchQueue.main.async {
                self.weatherImageView.image = image
            }
        }
        
    }
}
