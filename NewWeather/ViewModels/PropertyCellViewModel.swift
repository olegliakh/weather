import Foundation

class PropertyCellViewModel {
    
    var property = Bindable<String?>(nil)
    var result = Bindable<String?>(nil)
    
    func configure(withResult result: String, forName name : String) {
        self.property.value = name
        self.result.value = result
    }
    
}
