import CoreLocation
import Foundation

class CityCellViewModel {
    var name = Bindable<String?>(nil)
//    var latitude = Bindable<Double?>(nil)
//    var longitude = Bindable<Double?>(nil)
//    var location = Bindable<CLLocationCoordinate2D?>(nil)
    
//    func configure(name: String, latitude: Double, longitude: Double) {
//        print("I'm in city vm")
//        self.name.value = name
//        self.latitude.value = latitude
//        self.longitude.value = longitude
//    }
    func configure(name: String) {
        self.name.value = name
    }
    
//    func configure(name: String, location: CLLocationCoordinate2D) {
//        print("I'm in city vm")
//        self.name.value = name
//        self.location.value = location
//    }
}
