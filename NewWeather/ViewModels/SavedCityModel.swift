import Foundation

class SavedCityModel {
    var name = Bindable<String?>(nil)
    
    func configure(name: String) {
        self.name.value = name
    }
    
}
