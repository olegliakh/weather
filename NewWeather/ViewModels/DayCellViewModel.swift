import Foundation
import UIKit
import Kingfisher

class DayCellViewModel {
    
    var day = Bindable<String?>(nil)
    var dayTemp = Bindable<Int?>(nil)
    var nightTemp = Bindable<Int?>(nil)
    
    var dayImage = Bindable<UIImage?>(nil)
    var nightImage = Bindable<UIImage?>(nil)
    
    func configure(withInfo weather: Daily) {
        self.day.value = Double(weather.dt ?? 0).getDayOfWeekFromUTC()
        self.dayTemp.value = Int(weather.temp?.day ?? 0)
        self.nightTemp.value = Int(weather.temp?.night ?? 0)
        
        let urlString = "https://openweathermap.org/img/wn/\(weather.weather?.first?.icon ?? "").png"
        if let url = URL(string: urlString) {
            KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, downloadTaskUpdated: nil) { result in
                self.dayImage.value = try? result.get().image
            }
        }
    }
    
}
