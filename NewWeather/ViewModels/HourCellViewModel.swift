import Foundation
import UIKit
import Kingfisher


class HourCellViewModel {
    var hour = Bindable<String?>(nil)
    var temp = Bindable<String?>(nil)
    var image = Bindable<UIImage?>(nil)
    
    func configure(withInfo weather: Current, cityTimeOffset: Int, myTimeOffset: Int) {
        
        self.hour.value = Double((weather.dt ?? 0) + cityTimeOffset - myTimeOffset).getTimeFromUTC()
        self.temp.value = String(Int(weather.temp ?? 0)) + "°"
        
        let newURLString = "https://openweathermap.org/img/wn/\(weather.weather?.first?.icon ?? "").png"
        if let url = URL(string: newURLString) {
            KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, downloadTaskUpdated: nil) { result in
                self.image.value = try? result.get().image
            }
        }
    }
}
