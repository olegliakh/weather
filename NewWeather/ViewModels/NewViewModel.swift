import Foundation
import CoreLocation

class NewModel {
    
    var location = Bindable<CLLocationCoordinate2D?>(nil)
    var weather = Bindable<WeatherData?>(nil)
    var cityName = Bindable<String>("")
    
    var numberOfHourItems = 24
    var numberOfSections = 2
    var numberOfRwPerSection : [Int] = [7, 5]
    
    let myTimeOffset = (TimeZone.current.secondsFromGMT())
    
    var savedCities = [Bindable<City?>(nil)]
    
    func addCity(newCity: City) {
        if !savedCities.contains(where: { oneCity in
            oneCity.value?.name == newCity.name
        }) {
            let city = Bindable<City?>(nil)
            city.value = newCity
            savedCities.append(city)
        }
    }
    func loadSavedCities() {
        if let citiesArray = UserDefaults.standard.value([String].self, forKey: "saved cities") {
            savedCities
            
        }
    }
    func setLocation(location: CLLocationCoordinate2D) {
        self.location.value = location
    }
    
    func setCityName(_ name: String) {
        print("setting name in viewmodel")
        cityName.value = name
        WeatherManager.shared.getCoordinates(byName: name) { lat, lon in
            self.location.value = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            self.loadWeatherData()
        }
        
        
    }
    func loadCityName() {
        if let location = location.value {
            print("Loading name")
            WeatherManager.shared.getCityName(byCoordinates: location) { cityName in
                self.cityName.value = cityName
            }
        }
    }
    
    func loadWeatherData() {
        if let location = location.value {
            print("Loading data")
            WeatherManager.shared.fetchWeather(oneCity: location) { weather in
                self.weather.value = weather
            }
        }
    }
}
