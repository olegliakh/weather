import CoreLocation
import Foundation

class SearchViewModel {
    
    var filteredCity = Bindable<[String?]>([nil])
    var loadedCities = Bindable<[String?]>([nil])
    
    func loadCitiesFromApi () {
        WeatherManager.shared.loadCities { cities in
            print("Count of cities is \(cities.count)")
            self.loadedCities.value.append(contentsOf: cities)
        }
    }
}
