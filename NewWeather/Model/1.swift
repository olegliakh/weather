//// This file was generated from JSON Schema using quicktype, do not modify it directly.
//// To parse the JSON, add this file to your project and do:
////
////   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)
//
//import Foundation
//
//// MARK: - Welcome
//class WeatherData: Codable {
//    let lat, lon: Double?
//    let timezone: String?
//    let timezoneOffset: Int?
//    let current: Current?
//    let minutely: [Minutely]?
//    let hourly: [Current]?
//    let daily: [Daily]?
//    let alerts: [Alert]?
//
//    enum CodingKeys: String, CodingKey {
//        case lat, lon, timezone
//        case timezoneOffset = "timezone_offset"
//        case current, minutely, hourly, daily, alerts
//    }
//}
//
//// MARK: - Alert
//class Alert: Codable {
//    let senderName, event: String?
//    let start, end: Int?
//    let alertDescription: String?
//    let tags: [String]?
//
//    enum CodingKeys: String, CodingKey {
//        case senderName = "sender_name"
//        case event, start, end
//        case alertDescription = "description"
//        case tags
//    }
//}
//
////enum Tag: String, Codable {
////    case clouds = "Clouds"
////    case rain = "Rain"
////    case snow = "Snow"
////}
//
//// MARK: - Current
//class Current: Codable {
//    let dt, sunrise, sunset: Int?
//    let temp, feelsLike: Double?
//    let pressure, humidity: Int?
//    let dewPoint, uvi: Double?
//    let clouds, visibility: Int?
//    let windSpeed: Double?
//    let windDeg: Int?
//    let windGust: Double?
//    let weather: [Weather]?
//    let pop: Double?
//    let rain: Rain?
//
//    enum CodingKeys: String, CodingKey {
//        case dt, sunrise, sunset, temp
//        case feelsLike = "feels_like"
//        case pressure, humidity
//        case dewPoint = "dew_point"
//        case uvi, clouds, visibility
//        case windSpeed = "wind_speed"
//        case windDeg = "wind_deg"
//        case windGust = "wind_gust"
//        case weather, pop, rain
//    }
//}
//
//// MARK: - Rain
//class Rain: Codable {
//    let the1H: Double?
//
//    enum CodingKeys: String, CodingKey {
//        case the1H = "1h"
//    }
//}
//
//// MARK: - Weather
//class Weather: Codable {
//    let id: Int?
//    let main: String?
//    let weatherDescription: String?
//    let icon: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id, main
//        case weatherDescription = "description"
//        case icon
//    }
//}
//
////enum Icon: String, Codable {
////    case the03D = "03d"
////    case the04D = "04d"
////    case the04N = "04n"
////    case the10D = "10d"
////    case the10N = "10n"
////    case the13D = "13d"
////}
//
////enum Description: String, Codable {
////    case lightRain = "light rain"
////    case moderateRain = "moderate rain"
////    case overcastClouds = "overcast clouds"
////    case rainAndSnow = "rain and snow"
////    case scatteredClouds = "scattered clouds"
////}
//
//// MARK: - Daily
//class Daily: Codable {
//    let dt, sunrise, sunset, moonrise: Int?
//    let moonset: Int?
//    let moonPhase: Double?
//    let temp: Temp?
//    let feelsLike: FeelsLike?
//    let pressure, humidity: Int?
//    let dewPoint, windSpeed: Double?
//    let windDeg: Int?
//    let windGust: Double?
//    let weather: [Weather]?
//    let clouds: Int?
//    let pop, rain, uvi, snow: Double?
//
//    enum CodingKeys: String, CodingKey {
//        case dt, sunrise, sunset, moonrise, moonset
//        case moonPhase = "moon_phase"
//        case temp
//        case feelsLike = "feels_like"
//        case pressure, humidity
//        case dewPoint = "dew_point"
//        case windSpeed = "wind_speed"
//        case windDeg = "wind_deg"
//        case windGust = "wind_gust"
//        case weather, clouds, pop, rain, uvi, snow
//    }
//}
//
//// MARK: - FeelsLike
//class FeelsLike: Codable {
//    let day, night, eve, morn: Double?
//}
//
//// MARK: - Temp
//class Temp: Codable {
//    let day, min, max, night: Double?
//    let eve, morn: Double?
//}
//
//// MARK: - Minutely
//class Minutely: Codable {
//    let dt, precipitation: Int?
//}
//
