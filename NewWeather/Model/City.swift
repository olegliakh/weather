import CoreLocation
import Foundation

class City { //}: Codable {
    var name : String?
//    var location : CLLocationCoordinate2D?
    var latitude: Double?
    var longitude: Double?
    
    init(name: String, latitude: Double, longitude: Double) {
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
    }
    
//    init(name: String, location : CLLocationCoordinate2D) {
//        self.name = name
//        self.location = location
//    }
//
//    private enum CodingKeys: String, CodingKey {
//        case name
//        case location
//    }
//    
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//        try container.encode(location, forKey: .location)
//        try container.encode(name, forKey: .name)
//    }
//    
//    required init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        self.location = try container.decodeIfPresent(CLLocationCoordinate2D.self, forKey: .location)
//        self.name = try container.decodeIfPresent(String.self, forKey: .name)
//    }
}
