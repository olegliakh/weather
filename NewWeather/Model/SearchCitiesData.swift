import Foundation

// MARK: - Welcome
class SearchCitiesData: Codable {
    let error: Bool?
    let msg: String?
    let data: [Datum]?
}

// MARK: - Datum
class Datum: Codable {
    let iso2, iso3, country: String?
    let cities: [String]?
}
