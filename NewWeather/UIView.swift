import Foundation
import UIKit

extension UIView {
    func dropShadow() {
           self.layer.masksToBounds = false
           self.layer.shadowColor =  UIColor.black.cgColor
           self.layer.shadowOpacity = 0.9
           self.layer.shadowOffset = CGSize(width: 0, height: 0)
           self.layer.shadowRadius = 10
           self.layer.shouldRasterize = true //пикселизация тени
//           self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath // для рисования внутри вью кривые безье
        self.layer.cornerRadius = 20
       }
    func addShadow() {
    self.layer.cornerRadius = 10

    // border
//        self.layer.borderWidth = 1.0
//        self.layer.borderColor = UIColor.black.cgColor

    // shadow
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 4.0
        
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 10).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}


