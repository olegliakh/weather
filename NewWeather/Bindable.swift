import Foundation

class Bindable<T> { //generic
    typealias Listener = (T) -> Void
    private var listener: Listener? // ссылка на опциональное замыкание
    
    var value: T {
        didSet {            //KVO наблюдение за ключевым значением/ при изменении вэлью происходит вызов замыкания/ вместо Т новое значение
            listener?(value)
        }
    }
    
    init(_ value: T) { // конструктор
        self.value = value //меняет вэлью
    }
    
    /**
     - Important:
     Best pratice is to only set any UI attribute in a single binding. Failing to follow
     that suggestion can result in hard to track bugs where the order that values are set results in
     different UI outcomes.
     
     - Parameters:
     - listener: The *closure* to execute when respond to value changes.
     */
    func bind(_ listener: Listener?) {
        self.listener = listener // меняет замыкание
        listener?(value)    //вызывает его(отправляет вэлью)
    }
}

