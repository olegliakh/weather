import UIKit

class DayTableViewCell: UITableViewCell {
    
    var viewModel = DayCellViewModel()
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dayImageView: UIImageView!
    @IBOutlet weak var dayTempLabel: UILabel!
    @IBOutlet weak var nightImageView: UIImageView!
    @IBOutlet weak var nightTempLabel: UILabel!
    
    func bind() {
        self.viewModel.day.bind { day in
            DispatchQueue.main.async {
                self.dayLabel.text = day
            }
        }
        self.viewModel.dayTemp.bind { _ in
            DispatchQueue.main.async {
                self.dayTempLabel.text = String(self.viewModel.dayTemp.value ?? 0) + "°"
            }
        }
        self.viewModel.nightTemp.bind { temp in
            DispatchQueue.main.async {
                self.nightTempLabel.text = String(self.viewModel.nightTemp.value ?? 0) + "°"
            }
        }
        self.viewModel.dayImage.bind { image in
            self.dayImageView.image = image
        }
    }
}
