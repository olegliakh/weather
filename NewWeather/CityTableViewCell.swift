
import UIKit
import CoreLocation

class CityTableViewCell: UITableViewCell {
    
    var viewModel = CityCellViewModel()
//    var location : CLLocationCoordinate2D?

    @IBOutlet weak var cityLabel: UILabel!
    
    func bind () {
        viewModel.name.bind { name in
            self.cityLabel.text = name
        }
//        viewModel.location.bind { location in
//            self.location = location
//        }
    }
}
